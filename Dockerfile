FROM python:3-alpine
WORKDIR /app
ENV user=admin
ENV email=admin@example.com
ENV password=pass 
COPY ./requirements.txt ./
RUN pip3 install -r requirements.txt \
    && mkdir db
COPY . .
VOLUME [ "/app/db" ]
EXPOSE 8000
CMD sh init.sh && python3 manage.py runserver 0.0.0.0:8000
